from setuptools import setup

setup(name='ColabGDrive',
      version='0.0.1',
      description='Functions for working with GDrive from Colaboratory',
      url='git+https://drdavidmrace@bitbucket.org/drdavidmrace/colabgdrive.git',
      author='Dr David Race',
      author_email='dr.david.race.teacher@gmail.com',
      license='MIT',
      packages=['ColabGDrive'],
      zip_safe=False)